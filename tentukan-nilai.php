<?php
function tentukan_nilai($int)
{
    //  kode disini
    $output = "";
    if ($int >= 85 && $int < 100 ) {
        $output .= "Nilai Sangat Baik <br>";
    } else if ($int >= 70 && $int < 85 ) {
        $output .= "Nilai Baik <br>";
    } else if ($int >= 60 && $int < 70 ) {
        $output .= "Nilai Cukup <br>";
    } else {
        $output .= "Nilai Kurang";
    }
    return $output;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>
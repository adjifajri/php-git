<?php
function ubah_huruf($string){
//kode di sini
$huruf = "abcdefghijklmnopqrstuvwxyz";
$out = "";

for ($i=0; $i < strlen($string); $i++) { 
    $pos = strrpos($huruf, $string[$i]);
    $out .= substr($huruf, $pos +1, 1);
}
return $out;
}

// TEST CASES
echo ubah_huruf('wow');
echo "<br>" ;// xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>" ;// xpx
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>" ;// xpx
echo ubah_huruf('keren'); // lfsfo
echo "<br>" ;// xpx
echo ubah_huruf('semangat'); // tfnbohbu

?>